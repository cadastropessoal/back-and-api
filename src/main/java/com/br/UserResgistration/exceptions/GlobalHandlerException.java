package com.br.UserResgistration.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalHandlerException extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ApiErrorException> handlerExceptionBadRequest(BusinessException ex) {
        ApiErrorException apiError = new ApiErrorException(ex.code, ex.message);
        return new ResponseEntity<ApiErrorException>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(BusinessUnauthorizedException.class)
    public ResponseEntity<ApiErrorException> handlerException401Request(BusinessUnauthorizedException ex) {
        ApiErrorException apiError = new ApiErrorException(ex.code, ex.message);
        return new ResponseEntity<ApiErrorException>(apiError, HttpStatus.UNAUTHORIZED);
    }
}
