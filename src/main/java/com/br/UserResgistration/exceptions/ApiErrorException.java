package com.br.UserResgistration.exceptions;

public class ApiErrorException {

    public String code;
    public String message;

    public ApiErrorException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
