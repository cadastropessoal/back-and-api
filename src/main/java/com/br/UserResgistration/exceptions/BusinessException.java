package com.br.UserResgistration.exceptions;

public class BusinessException extends RuntimeException  {

    String code;
    String message;

    public BusinessException(BusinessException be) {
        this.code = be.code;
        this.message = be.message;
    }

    public BusinessException(String code, String message) {
        this.code = code;
        this.message = message;
    }

}