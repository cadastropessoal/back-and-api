package com.br.UserResgistration.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.service.MessageService;

@Service
public class ErrorExceptionService {

    @Autowired
    private MessageService messageSource;

    public void showNotFound() throws BusinessException {
        this.errorApi("001");
    }

    public void showUnnamed() throws BusinessException {
        this.errorApi("002");
    }

    public void showEmptyCPF() throws BusinessException {
        this.errorApi("003");
    }

    public void showInvalidCPF() throws BusinessException {
        this.errorApi("004");
    }

    public void showAlreadyRegisteredCPF() throws BusinessException {
        this.errorApi("005");
    }

    public void showEmptyCEP() throws BusinessException {
        this.errorApi("006");
    }

    public void showInvalidCEP() throws BusinessException {
        this.errorApi("007");
    }

    public void showEmptyAge() throws BusinessException {
        this.errorApi("008");
    }

    public void showInsufficientAge() throws BusinessException {
        this.errorApi("009");
    }

    public void showUserDelete() throws BusinessException {
        this.errorApi("010");
    }

    public void showEmptyNeighborhood() throws BusinessException {
        this.errorApi("011");
    }

    public void showEmptyRoad() throws BusinessException {
        this.errorApi("012");
    }

    public void showEmptyNumber() throws BusinessException {
        this.errorApi("013");
    }

    public void showEmptyCity() throws BusinessException {
        this.errorApi("014");
    }

    public void showEmptyState() throws BusinessException {
        this.errorApi("015");
    }

    public void showAddressNotFound() throws BusinessException {
        this.errorApi("016");
    }

    public void showEmptyEmail() throws BusinessException {
        this.errorApi("017");
    }

    public void showAlreadyRegisteredEmail() throws BusinessException {
        this.errorApi("018");
    }

    public void showInvalidEmail() throws BusinessException {
        this.errorApi("019");
    }

    public void showCannotDeleteUser() throws BusinessException {
        this.errorApi("020");
    }

    public void showEmptyPassword() throws BusinessException {
        this.errorApi("021");
    }

    public void showInvalidPassword() throws BusinessException {
        this.errorApi("022");
    }

    public void showIncorrectPassword() throws BusinessException {
        this.errorApi("023");
    }

    public void showUnauthorized() throws BusinessException {
        this.errorApi("401");

    }

    public void showPageMinorZero() throws BusinessException {
        this.errorApi("024");
    }

    public void showPasswordconfirm() throws BusinessException {
        this.errorApi("025");
    }

    private void errorApi(String code) {
        throw new BusinessException(messageSource.getBusinessCodeMessage(code));
    }
}
