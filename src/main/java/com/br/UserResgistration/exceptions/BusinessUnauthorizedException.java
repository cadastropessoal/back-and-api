package com.br.UserResgistration.exceptions;

public class BusinessUnauthorizedException extends RuntimeException {
    String code;
    String message;

    public BusinessUnauthorizedException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
