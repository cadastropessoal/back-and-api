package com.br.UserResgistration.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.br.UserResgistration.settings.CustomAuthenticationEntryPoint;
import com.br.UserResgistration.settings.OncePerRequestFilterCustom;

@Configuration
@EnableMethodSecurity()
public class SecurityConfig {

        private final CustomAuthenticationEntryPoint authenticationEntryPoint;
        private final OncePerRequestFilterCustom oncePerRequestFilterCustom;

        public SecurityConfig(CustomAuthenticationEntryPoint authenticationEntryPoint,
                        OncePerRequestFilterCustom oncePerRequestFilterCustom) {
                this.authenticationEntryPoint = authenticationEntryPoint;
                this.oncePerRequestFilterCustom = oncePerRequestFilterCustom;
        }

        @Bean
        public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
                http.csrf(AbstractHttpConfigurer::disable)
                                .authorizeHttpRequests(auth -> auth
                                                .requestMatchers(
                                                                "/auth/**",
                                                                "/api-docs/**",
                                                                "/swagger-ui/**")
                                                .permitAll()
                                                .anyRequest().authenticated())
                                .exceptionHandling(
                                                exceptionHandling -> exceptionHandling
                                                                .authenticationEntryPoint(authenticationEntryPoint))
                                .addFilterBefore(oncePerRequestFilterCustom,
                                                UsernamePasswordAuthenticationFilter.class);

                return http.build();
        }
}
