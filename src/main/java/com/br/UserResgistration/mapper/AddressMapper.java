package com.br.UserResgistration.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.br.UserResgistration.dto.AddressDTO;
import com.br.UserResgistration.dto.AddressDetailDTO;
import com.br.UserResgistration.model.Addresses;

public class AddressMapper {
    public static Addresses toEntity(AddressDTO addressDTO) {
        Addresses address = new Addresses();
        address.setNeighborhood(addressDTO.getNeighborhood());
        address.setCep(addressDTO.getCep());
        address.setCity(addressDTO.getCity());
        address.setNumber(addressDTO.getNumber());
        address.setRoad(addressDTO.getRoad());
        address.setState(addressDTO.getState());
        return address;
    }

    public static AddressDetailDTO toDto(Addresses addresses) {
        AddressDetailDTO address = new AddressDetailDTO();
        address.setNeighborhood(addresses.getNeighborhood());
        address.setCep(addresses.getCep());
        address.setCity(addresses.getCity());
        address.setNumber(addresses.getNumber());
        address.setRoad(addresses.getRoad());
        address.setState(addresses.getState());
        address.setId(addresses.getId());
        return address;
    }

    public static List<AddressDetailDTO> toListDto(List<Addresses> addressList) {
        return addressList.stream()
                .map(AddressMapper::toDto)
                .collect(Collectors.toList());
    }
}
