package com.br.UserResgistration.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.br.UserResgistration.dto.AuthDTO;
import com.br.UserResgistration.dto.UserDTO;
import com.br.UserResgistration.dto.UserDetailAuthDTO;
import com.br.UserResgistration.dto.UserDetailDTO;
import com.br.UserResgistration.dto.UserRegisterDTO;
import com.br.UserResgistration.model.User;


public class UserMapper {

    public static User ToEntity(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setIdade(userDTO.getIdade() != null ? userDTO.getIdade().byteValue() : 0);
        user.setCpf(userDTO.getCpf());
        user.setEmail(userDTO.getEmail());
        return user;
    }

    public static UserDetailDTO ToEntityDetailDTO(User user) {
        UserDetailDTO userDTO = new UserDetailDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setIdade((int) user.getIdade());
        userDTO.setCpf(user.getCpf());
        userDTO.setEmail(user.getEmail());
        userDTO.setStatus(user.getStatus());
        userDTO.setProfileEnum(user.getProfile());

        return userDTO;
    }

    public static List<UserDetailDTO> toEntityList(List<User> userDTOList) {
        return userDTOList.stream()
                .map(UserMapper::ToEntityDetailDTO)
                .collect(Collectors.toList());
    }

    public static User toEntityRegister(UserRegisterDTO userLogin) {
        User user = new User();
        user.setName(userLogin.getName());
        user.setEmail(userLogin.getEmail());
        user.setPassword(userLogin.getPassword());

        return user;

    }
    

    public static UserRegisterDTO toEntityRegisterDTO(User userLogin) {
        UserRegisterDTO user = new UserRegisterDTO();
        user.setName(userLogin.getName());
        user.setEmail(userLogin.getEmail());
        user.setPassword(userLogin.getPassword());

        return user;

    }

    public static UserDetailAuthDTO toEntityRegister(User userlogin) {
        UserDetailAuthDTO user = new UserDetailAuthDTO();
        user.setName(userlogin.getName());
        user.setEmail(userlogin.getEmail());
        return user;
    }
    

    public static AuthDTO toAuthDto(User user) {
        AuthDTO dto = new AuthDTO();
        dto.setName(user.getName());
        dto.setEmail(user.getEmail());
        dto.setId(user.getId()); 
        dto.setProfile(user.getProfile());
       return dto;
    }

}
