package com.br.UserResgistration.settings;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.br.UserResgistration.enums.ProfileEnum;
import com.br.UserResgistration.enums.UserStatusEnum;
import com.br.UserResgistration.model.User;
import com.br.UserResgistration.repository.UserRepository;

@Configuration
public class DataInitializer {

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return args -> {
            if (userRepository.findByEmailIgnoreCase("admin@example.com").isEmpty()) {
                User user = new User();
                user.setName("Administrador");
                user.setEmail("admin@example.com");
                user.setPassword(encoder.encode("admin123")); // Senha criptografada
                user.setProfile(ProfileEnum.ADMIN);
                user.setUserStatusEnum(UserStatusEnum.ACTIVE);
                userRepository.save(user);

            }
        };
    }
}
