package com.br.UserResgistration.settings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.tags.Tag;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        String securitySchemeName = "BearerAuth";
        return new OpenAPI()
                .info(new Info()
                        .title("API Cadastro Pessoa Física")
                        .version("1.0.0")
                        .description("Documentação da API com suporte a autenticação JWT."))
                .components(new Components()
                        .addSecuritySchemes(securitySchemeName, new SecurityScheme()
                                .name(securitySchemeName)
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .bearerFormat("JWT")))
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .addTagsItem(new Tag().name("1. Authentication").description("Endpoints de autenticação"))
                .addTagsItem(new Tag().name("2. User").description("Endpoints de usuários"))
                .addTagsItem(new Tag().name("3. Address").description("Endpoints de endereços"))
                .addTagsItem(new Tag().name("4. Locale").description("Endpoints de endereços"));
    }
}
