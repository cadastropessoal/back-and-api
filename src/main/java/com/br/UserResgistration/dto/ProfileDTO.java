package com.br.UserResgistration.dto;

import com.br.UserResgistration.enums.ProfileEnum;

public class ProfileDTO {

    private ProfileEnum lang;

    public ProfileEnum getLang() {
        return lang;
    }

    public void setLang(ProfileEnum lang) {
        this.lang = lang;
    }

}
