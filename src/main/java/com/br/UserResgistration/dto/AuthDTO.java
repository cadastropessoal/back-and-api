package com.br.UserResgistration.dto;

import java.util.Date;

import com.br.UserResgistration.enums.ProfileEnum;

public class AuthDTO {

    private String name;
    private String email;
    private Integer id;
    private String acessToken;
    private ProfileEnum profile;
    private Date expiredDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcessToken() {
        return acessToken;
    }

    public void setAcessToken(String acessToken) {
        this.acessToken = acessToken;
    }

    public ProfileEnum getProfile() {
        return profile;
    }

    public void setProfile(ProfileEnum profile) {
        this.profile = profile;
    }

    public void setProfile(String profile) {
        if (ProfileEnum.ADMIN.toString().equals(profile)) {
            this.profile = ProfileEnum.ADMIN;
        } else if (ProfileEnum.USER.toString().equals(profile)) {
            this.profile = ProfileEnum.USER;
        }
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

}
