package com.br.UserResgistration.dto;

import com.br.UserResgistration.enums.LocaleTypeEnum;

public class LocaleDTO {

    private LocaleTypeEnum lang;

    public LocaleTypeEnum getLang() {
        return lang;
    }

    public void setLang(LocaleTypeEnum lang) {
        this.lang = lang;
    }

}
