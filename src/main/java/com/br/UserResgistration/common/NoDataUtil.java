package com.br.UserResgistration.common;

public class NoDataUtil {

  public static boolean noData(String value) {
    if (isNull(value) ||
        isEmpty(value) ||
        isBlank(value)) {
      return true;
    } else
      return false;
  }

  public static boolean isNull(Object a) {
    return a == null;
  }

  public static boolean isEmpty(String b) {
    return b.isEmpty();
  }

  public static boolean isTrimEmpty(String c) {
    return c.trim().isEmpty();
  }

  public static boolean isBlank(String d) {
    return d.isBlank();
  }
}