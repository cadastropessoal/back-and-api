package com.br.UserResgistration.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.UserResgistration.dto.LocaleDTO;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.service.MessageService;
import com.br.UserResgistration.validation.LocaleValidation;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "4. Locale")
@RequestMapping(value = "locale")
public class LocaleController {

    @Autowired
    private LocaleValidation localeValidation;

    @Autowired
    private MessageService messageService;

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(method = { RequestMethod.POST })
    public void post(@RequestBody LocaleDTO locale) throws BusinessException {

        localeValidation.validation(locale.getLang());

        messageService.setlocale(locale.getLang());

        System.out.println(Locale.getDefault());
    }
}
