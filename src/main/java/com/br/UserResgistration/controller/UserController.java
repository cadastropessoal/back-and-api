package com.br.UserResgistration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.br.UserResgistration.dto.PageResultDTO;
import com.br.UserResgistration.dto.UserDTO;
import com.br.UserResgistration.dto.UserDetailDTO;
import com.br.UserResgistration.enums.UserStatusEnum;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.model.UserFilter;
import com.br.UserResgistration.service.UserService;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController()
@Tag(name = "2. User")
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UserService service;

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "{id}", method = { RequestMethod.GET })
    public UserDetailDTO getUser(@PathVariable("id") Integer id) throws BusinessException {
        return this.service.getUserById(id);
    }

    @PreAuthorize("hasRole('ADMIN') or (hasRole('USER') and #id == authentication.principal)")
    @RequestMapping(value = "{id}", method = { RequestMethod.PUT })
    public UserDetailDTO putPessoaFisica(@PathVariable Integer id, @RequestBody UserDTO user)
            throws BusinessException {

        return this.service.update(id, user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "{id}", method = { RequestMethod.DELETE })
    public void deletePessoaFisica(@PathVariable("id") Integer id) throws BusinessException {
        this.service.delete(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "all-pagination", method = { RequestMethod.GET })
    public PageResultDTO<UserDetailDTO> getPageUser(
            @RequestParam(required = true, defaultValue = "1") Integer page,
            @RequestParam(required = true, defaultValue = "10") Integer perPage,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String cpf,
            @RequestParam(required = false) UserStatusEnum status) {

        UserFilter filter = new UserFilter();
        filter.setPage(page);
        filter.setPerPage(perPage);
        filter.setCPF(cpf);
        filter.setName(name);
        filter.setStatus(status);

        return service.getListUserPage(filter);
    }

    @PreAuthorize("hasRole('ADMIN') or (hasRole('USER') and #id == authentication.principal)")
    @RequestMapping(value = "{id}/update-password", method = { RequestMethod.PUT })
    public void changePassword(
            @RequestParam(required = true, defaultValue = "10") @PathVariable("id") Integer id,
            @RequestParam(required = true) String currentPassword,
            @RequestParam(required = true) String newPassword) {

        service.changePasswordService(id, currentPassword, newPassword);
    }

}
