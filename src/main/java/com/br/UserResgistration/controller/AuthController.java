package com.br.UserResgistration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.br.UserResgistration.dto.AuthDTO;
import com.br.UserResgistration.dto.UserDetailAuthDTO;
import com.br.UserResgistration.dto.UserLoginDTO;
import com.br.UserResgistration.dto.UserRegisterDTO;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.service.AuthService;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController()
@RequestMapping(value = "auth")
@Tag(name = "1. Authentication", description = "Authentication related endpoints")
public class AuthController {

    @Autowired
    private AuthService service;

    @RequestMapping(path = "/register", method = { RequestMethod.POST })
    public UserDetailAuthDTO register(@RequestBody UserRegisterDTO user) throws BusinessException {
        return service.register(user);
    }

    @RequestMapping(path = "/login", method = { RequestMethod.POST })
    public AuthDTO login(@RequestBody UserLoginDTO user) throws BusinessException {
        return service.login(user);
    }

     @RequestMapping(value = "/recover-password", method = { RequestMethod.POST })
    public void postEmailRecoverPassword(@RequestParam(required = true) String email) {

        service.recoverPassword(email);
    }

    @RequestMapping(value = "/reset-password", method = { RequestMethod.PUT })
    public void putPasswordToken(@RequestParam(required = true) String email, String token, String newPassword) {

        service.resetPasswordToken(email, token, newPassword);
    }
}
