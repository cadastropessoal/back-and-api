package com.br.UserResgistration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.UserResgistration.dto.AddressDTO;
import com.br.UserResgistration.dto.AddressDetailDTO;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.service.AddressService;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController()
@RequestMapping(value = "address")
@Tag(name = "3. Address", description = "Address related endpoints")

public class AddressController {

    @Autowired
    private AddressService service;

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "{id}", method = { RequestMethod.GET })
    public AddressDetailDTO getAddress(@PathVariable("id") Integer idAddress) throws BusinessException {
        return this.service.getAddress(idAddress);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "add-user/{idUser}", method = { RequestMethod.POST })
    public AddressDetailDTO postPessoaFisica(@PathVariable("idUser") Integer idUser, @RequestBody AddressDTO address)
            throws BusinessException {
        return this.service.save(idUser, address);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "{id}", method = { RequestMethod.PUT })
    public AddressDetailDTO putAddresses(@PathVariable("id") Integer idAddress, @RequestBody AddressDTO address)
            throws BusinessException {
        return this.service.putAddress(idAddress, address);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "{id}", method = { RequestMethod.DELETE })
    public ResponseEntity<AddressDetailDTO> deleteAddress(@PathVariable("id") Integer idAddress)
            throws BusinessException {

        this.service.deleteAddress(idAddress);

        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @RequestMapping(value = "all-user/{idUser}", method = { RequestMethod.GET })
    public List<AddressDetailDTO> getAddresses(@PathVariable("idUser") Integer idUser) throws BusinessException {
        return this.service.getListAddresses(idUser);
    }
}