package com.br.UserResgistration.enums;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema()
public enum LocaleTypeEnum {
    PT_BR,
    EN_US;
}
