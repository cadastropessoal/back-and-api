package com.br.UserResgistration.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.br.UserResgistration.enums.UserStatusEnum;
import com.br.UserResgistration.model.User;

public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    Optional<User> findByCpf(String cpf);

    Optional<User> findByEmailIgnoreCase(String email);

    Optional<User> findByEmailIgnoreCaseAndStatus(String email, UserStatusEnum status);

    Page<User> findAllByNameAndCpfAndStatus(String name, String cpf, UserStatusEnum status,
            Pageable pageable);

    Page<User> findAll(Pageable pageable);

}