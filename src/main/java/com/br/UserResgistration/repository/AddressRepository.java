package com.br.UserResgistration.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.UserResgistration.model.Addresses;

public interface AddressRepository extends JpaRepository<Addresses, Integer> {

    List<Addresses> findByUserId(Integer userId);
}
