package com.br.UserResgistration.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.common.NoDataUtil;
import com.br.UserResgistration.dto.AddressDTO;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.exceptions.ErrorExceptionService;

@Service
public class AdressesValidation {

    @Autowired
    private ErrorExceptionService messagesCodeService;

    public void validation(AddressDTO adress) throws BusinessException {
        this.neighborhoodValidation(adress.getNeighborhood());
        this.roadValidation(adress.getRoad());
        this.numberValidation(adress.getNumber());
        this.cityValidation(adress.getCity());
        this.stateValidation(adress.getState());
        this.cepValidation(adress.getCep());
    }

    public void neighborhoodValidation(String neighborhood) throws BusinessException {
        if (NoDataUtil.noData(neighborhood)) {
            messagesCodeService.showEmptyNeighborhood();
        }
    }

    public void roadValidation(String road) throws BusinessException {
        if (NoDataUtil.noData(road)) {
            messagesCodeService.showEmptyRoad();
        }
    }

    public void numberValidation(String number) throws BusinessException {
        if (NoDataUtil.noData(number)) {

        }
    }

    public void cityValidation(String city) throws BusinessException {
        if (NoDataUtil.noData(city)) {
            messagesCodeService.showEmptyCity();
        }
    }

    public void stateValidation(String state) throws BusinessException {
        if (NoDataUtil.noData(state)) {
            messagesCodeService.showEmptyState();
        }
    }

    public ErrorExceptionService getMessagesCodeService() {
        return messagesCodeService;
    }

    public void setMessagesCodeService(ErrorExceptionService messagesCodeService) {
        this.messagesCodeService = messagesCodeService;
    }

    public void cepValidation(String cep) throws BusinessException {
        if (NoDataUtil.noData(cep)) {

            messagesCodeService.showEmptyCEP();
        }

        if (cep.length() != 8) {
            messagesCodeService.showInvalidCEP();
        }
    }

}