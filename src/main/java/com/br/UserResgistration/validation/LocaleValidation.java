package com.br.UserResgistration.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.enums.LocaleTypeEnum;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.exceptions.ErrorExceptionService;

@Service
public class LocaleValidation {

    @Autowired
    private ErrorExceptionService messagesCodeService;

    public void validation(LocaleTypeEnum lang) throws BusinessException { 
        this.validationLanguage(lang.toString());
    }

    private void validationLanguage(String lang) throws BusinessException {

        if (!lang.equals("PT_BR") && !lang.equals("EN_US")) {
            messagesCodeService.showNotFound();
        }
    }
}
