package com.br.UserResgistration.validation;

import org.springframework.data.jpa.domain.Specification;

import com.br.UserResgistration.enums.UserStatusEnum;
import com.br.UserResgistration.model.User;

public class UserSpecifications {

    public static Specification<User> hasName(String name) {
        return (root, query, criteriaBuilder) -> name != null ? criteriaBuilder.like(root.get("name"), "%" + name + "%")
                : null;
    }

    public static Specification<User> hasCpf(String cpf) {
        return (root, query, criteriaBuilder) -> cpf != null ? criteriaBuilder.equal(root.get("cpf"), cpf) : null;
    }

    public static Specification<User> hasStatus(UserStatusEnum status) {
        return (root, query, criteriaBuilder) -> status != null ? criteriaBuilder.equal(root.get("status"), status)
                : null;
    }
}