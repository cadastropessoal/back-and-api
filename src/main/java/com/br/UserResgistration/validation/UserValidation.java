package com.br.UserResgistration.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.common.NoDataUtil;
import com.br.UserResgistration.dto.UserDTO;
import com.br.UserResgistration.enums.ProfileEnum;
import com.br.UserResgistration.enums.UserStatusEnum;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.exceptions.ErrorExceptionService;

@Service
public class UserValidation {

    @Autowired
    private ErrorExceptionService errorService;

    public void validate(UserDTO userInput) throws BusinessException {
        this.validateUserName(userInput.getName());
        this.validateUserCPF(userInput.getCpf());
        this.validateUserAge(userInput.getIdade().byteValue());
        this.validateUserEmail(userInput.getEmail());
    }

    @Override
    public String toString() {
        return "UserValidation []";
    }

    public void validateUserName(String name) throws BusinessException {
        if (NoDataUtil.noData(name)) {
            errorService.showUnnamed();
        }
    }

    public void validateUserCPF(String cpf) throws BusinessException {
        if (NoDataUtil.noData(cpf)) {
            errorService.showEmptyCPF();
        }

        if (cpf.length() != 11) {
            errorService.showInvalidCPF();
        }
    }

    public void validateUserAge(byte age) throws BusinessException {
        if (NoDataUtil.isNull(age)) {
            errorService.showEmptyAge();
        }

        if (age < 18) {
            errorService.showInsufficientAge();
        }
    }

    public void validateUserEmail(String email) throws BusinessException {
        if (NoDataUtil.isNull(email)) {
            errorService.showEmptyEmail();
        }

        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            errorService.showInvalidEmail();
        }
    }

    public void validationProfile(ProfileEnum profileEnum) throws BusinessException {
        if (profileEnum != ProfileEnum.ADMIN && profileEnum != ProfileEnum.USER) {
            errorService.showNotFound();
        }
    }

    public void validateUserStatusEnum(UserStatusEnum statusEnum) throws BusinessException {
        if (statusEnum != UserStatusEnum.ACTIVE && statusEnum != UserStatusEnum.INACTIVE) {
            errorService.showNotFound();
        }
    }

    public void validateUserPassword(String password) throws BusinessException {
        if (NoDataUtil.isNull(password)) {
            errorService.showEmptyPassword();
        }

        String passwordRegex = "^(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>/?]).{8,}$";
        Pattern pattern = Pattern.compile(passwordRegex);
        Matcher matcher = pattern.matcher(password);

        if (!matcher.matches()) {
            errorService.showUnauthorized();
        }
    }

    public void validateCurrentPassword(String userPassword, String currentPassword) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(userPassword, currentPassword)) {
            errorService.showIncorrectPassword();
            return;
        }
    }
}
