package com.br.UserResgistration.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.dto.PageResultDTO;
import com.br.UserResgistration.dto.UserDTO;
import com.br.UserResgistration.dto.UserDetailDTO;
import com.br.UserResgistration.enums.UserStatusEnum;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.exceptions.ErrorExceptionService;
import com.br.UserResgistration.mapper.UserMapper;
import com.br.UserResgistration.model.User;
import com.br.UserResgistration.model.UserFilter;
import com.br.UserResgistration.repository.UserRepository;
import com.br.UserResgistration.validation.UserSpecifications;
import com.br.UserResgistration.validation.UserValidation;

import jakarta.validation.constraints.NotNull;

@Service
public class UserService extends GenericAbstractServiceImpl {

    @Autowired
    private UserValidation validation;

    @Autowired
    private ErrorExceptionService messagesCodeService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private HashService hashService;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    // CREATE
    public UserDetailDTO create(@NotNull UserDTO userDTO) throws BusinessException {

        validation.validate(userDTO);

        this.checkIfAlreadyRegistered(userDTO);

        return UserMapper.ToEntityDetailDTO(
                saveRepository(UserMapper.ToEntity(userDTO)));
    }

    public User saveRepository(User user) {
        return userRepository.save(user);
    }

    public void recoverPassword(String email) {

        var user = this.getCheckingIfExistUserbyEmail(email);
        this.mailService.sendEmailRecoverPassword(email, hashService.generateResetHash(user.getId()), user.getName());

    }

    // GETTES
    public List<UserDetailDTO> getListUser() {

        List<User> users = userRepository.findAll();
        return UserMapper.toEntityList(users);
    }

    public UserDetailDTO getUserById(Integer id) throws BusinessException {
        var result = getUserCheckingExistByID(id);

        UserDetailDTO user = UserMapper.ToEntityDetailDTO(result);

        return user;
    }

    private User getUserCheckingExistByID(Integer userId) {
        var entity = userRepository.findById(userId);
        User resultUser = null;
        if (entity.isPresent()) {
            resultUser = entity.get();
        } else {
            messagesCodeService.showNotFound();
        }
        return resultUser;
    }

    public User getCheckingIfExistUserbyEmail(String email) {
        validation.validateUserEmail(email);
        var option = userRepository.findByEmailIgnoreCaseAndStatus(email, UserStatusEnum.ACTIVE);
        if (!option.isPresent()) {
            messagesCodeService.showUnauthorized();
        }
        return option.get();
    }

    public PageResultDTO<UserDetailDTO> getListUserPage(UserFilter filter) {

        if (filter.getPage() <= 0 || filter.getPerPage() <= 0) {
            messagesCodeService.showPageMinorZero();
        }

        PageResultDTO<UserDetailDTO> pages = new PageResultDTO<>();

        Pageable pageable = PageRequest.of(filter.getPage() - 1, filter.getPerPage());
        Specification<User> spec = Specification
                .where(UserSpecifications.hasName(filter.getName()))
                .and(UserSpecifications.hasCpf(filter.getCPF()))
                .and(UserSpecifications.hasStatus(filter.getStatus()));
        Page<User> userPage = userRepository.findAll(spec, pageable);

        List<User> users = userPage.getContent();
        pages.setPage(filter.getPage());
        pages.setPerPage(filter.getPerPage());
        pages.setTotalItems(userPage.getTotalElements());
        pages.setNumberOfPage(userPage.getTotalPages());
        pages.setData(UserMapper.toEntityList(users));

        return pages;
    }

    // UPDATE
    public UserDetailDTO update(Integer id, UserDTO entrada) throws BusinessException {
        User putUser = this.getUserCheckingExistByID(id);

        var idade = entrada.getIdade() != null ? entrada.getIdade().byteValue() : null;

        updateField(putUser::getName, entrada::getName, putUser::setName, this.validation::validateUserName);
        updateField(putUser::getIdade, () -> idade, putUser::setIdade, this.validation::validateUserAge);
        updateField(putUser::getCpf, entrada::getCpf, putUser::setCpf, this.validation::validateUserCPF);
        updateField(putUser::getEmail, entrada::getEmail, putUser::setEmail, this.validation::validateUserEmail);

        // fiz essa parte para fazer verificação se é ADMIN.
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean isAdmin = auth.getAuthorities()
                .contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
        // AI AQUI NESSA PARTE ACRESCENTA SO PARA ADMIN
        if (isAdmin) {
            updateEnumField(putUser::getProfile, entrada::getProfileEnum, putUser::setProfile,
                    this.validation::validationProfile);
            updateEnumField(putUser::getStatus, entrada::getStatus, putUser::setUserStatusEnum,
                    this.validation::validateUserStatusEnum);
        }

        var newUser = saveRepository(putUser);
        return UserMapper.ToEntityDetailDTO(newUser);
    }

    public void changePasswordService(Integer id, String currentPassword, String newPassword) {
        User user = this.getUserCheckingExistByID(id);

        validation.validateCurrentPassword(user.getPassword(), currentPassword);

        user.setPassword(encoder.encode(newPassword));

        messagesCodeService.showPasswordconfirm();
    }

    public void resetPasswordToken(String email, String hash, String newPassword) {

        this.validation.validateUserPassword(newPassword);
        User user = this.getCheckingIfExistUserbyEmail(email);
        if (!this.hashService.validateResetHash(hash, user.getId())) {
            this.messagesCodeService.showUnauthorized();
        }
        this.mailService.sendEmailConfirmPassword(email, user.getName());

    }

    // DELETE
    public void delete(Integer id) throws BusinessException {

        this.getUserCheckingExistByID(id);

        try {

            userRepository.deleteById(id);

        } catch (DataIntegrityViolationException exception) {
            System.out.println(exception);
            messagesCodeService.showCannotDeleteUser();
        }

    }

    // VALIDATION
    private void checkIfAlreadyRegistered(UserDTO userDTO) throws BusinessException {
        if (userRepository.findByCpf(userDTO.getCpf()).isPresent()) {
            messagesCodeService.showAlreadyRegisteredCPF();
        }

        this.checkIfEmailIsAlreadyRegistered(userDTO.getEmail());

    }

    public void checkIfEmailIsAlreadyRegistered(String email) {
        validation.validateUserEmail(email);// aqui
        if (userRepository.findByEmailIgnoreCase(email).isPresent()) {
            messagesCodeService.showAlreadyRegisteredEmail();
        }
    }

    public void validation(User user) {
        validation.validateUserPassword(user.getPassword());
    }

    public void validationPassword(String currentPassword, String newPassword) {
        validation.validateCurrentPassword(currentPassword, newPassword);
    }
}
