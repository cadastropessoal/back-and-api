package com.br.UserResgistration.service;

import java.time.Instant;
import java.util.Random;

import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HashService {
    @Value("${hash.secret.key}")
    private String SALT;

    @Value("${jwt.expiration-time}")
    private long EXPIRATION_TIME_SECONDS;

    private int HASH_LENGTH = 8;

    public String generateResetHash(Integer idUser) {
        Random random = new Random();
        int code = 100000 + random.nextInt(900000);
        long timestamp = Instant.now().getEpochSecond();

        Hashids hashids = new Hashids(SALT, HASH_LENGTH);
        return hashids.encode(idUser, code, timestamp);
    }

    public boolean validateResetHash(String hash, long userId) {
        Hashids hashids = new Hashids(SALT, HASH_LENGTH);
        long[] decoded = hashids.decode(hash);

        if (decoded.length != 3) {
            return false; // O hash não contém os 3 valores esperados (ID, código e timestamp)
        }

        long idFromHash = decoded[0];
        long timestamp = decoded[2];

        if (idFromHash != userId) {
            return false; // O ID não corresponde ao do usuário
        }

        long currentTime = Instant.now().getEpochSecond();
        return (currentTime - timestamp) <= EXPIRATION_TIME_SECONDS; // Verifica se o código não expirou
    }



}
