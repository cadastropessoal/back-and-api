package com.br.UserResgistration.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.dto.AuthDTO;
import com.br.UserResgistration.enums.ProfileEnum;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.exceptions.ErrorExceptionService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@Service
public class JwtService {
    @Autowired
    private ErrorExceptionService errorExceptionService;

    @Value("${jwt.secret.key}")
    private String base64EncodedSecretKey;
    @Value("${jwt.expiration-time}")
    private long timeExpirationToken;

    public String generateToken(Integer id, String email, String username, ProfileEnum profile) {

        long expirationTime = timeExpirationToken;
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + expirationTime);

        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("id", id);
        claims.put("email", email);
        claims.put("username", username);
        claims.put("profile", profile);

        SecretKey jwtSecretKey = Keys
                .hmacShaKeyFor(base64EncodedSecretKey.getBytes());

        return Jwts.builder()
                .claims(claims)
                .issuedAt(now)
                .expiration(expirationDate)
                .signWith(jwtSecretKey)
                .compact();
    }

    public Boolean isTokenExpired(String token) {

        var payload = this.extractPayload(token);
        return payload.getExpiredDate().before(new Date());
    }

    public AuthDTO extractPayload(String token) {
        SecretKey jwtSecretKey = Keys.hmacShaKeyFor(base64EncodedSecretKey.getBytes());
        try {
            Claims claims = Jwts.parser()
                    .verifyWith(jwtSecretKey)
                    .build()
                    .parseSignedClaims(token)
                    .getPayload();

            AuthDTO dto = new AuthDTO();
            var profile = claims.get("profile", String.class);
            dto.setId(claims.get("id", Integer.class));
            dto.setProfile(profile);
            dto.setName(claims.get("username", String.class));
            dto.setExpiredDate(claims.get("exp", Date.class));

            return dto;
        } catch (BusinessException e) {
            this.errorExceptionService.showUnauthorized();
            return null;
        }

    }

    public String generateTokenRecover() {

        return null;
    }
}
