package com.br.UserResgistration.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.common.NoDataUtil;
import com.br.UserResgistration.enums.LocaleTypeEnum;
import com.br.UserResgistration.exceptions.ApiErrorException;
import com.br.UserResgistration.exceptions.BusinessException;

@Service
public class MessageService {

    @Autowired
    private MessageSource messageSource;

    public void setlocale(LocaleTypeEnum locale) throws BusinessException {

        Locale.setDefault(new Locale(locale.toString()));

    }

    public BusinessException getBusinessCodeMessage(String key) {

        var message = messageSource
                .getMessage(key, null, Locale.getDefault());

        String code = null;

        if (!NoDataUtil.noData(message)) {
            code = key.substring(0, 3);
        }

        return new BusinessException(code, message);
    }

    public ApiErrorException getCodeMessage(String key) {

        var message = messageSource
                .getMessage(key, null, Locale.getDefault());

        String code = null;

        if (!NoDataUtil.noData(message)) {
            code = key.substring(0, 3);
        }

        return new ApiErrorException(code, message);
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

}
