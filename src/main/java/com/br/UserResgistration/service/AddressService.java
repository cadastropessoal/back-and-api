package com.br.UserResgistration.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.dto.AddressDTO;
import com.br.UserResgistration.dto.AddressDetailDTO;
import com.br.UserResgistration.exceptions.BusinessException;
import com.br.UserResgistration.exceptions.ErrorExceptionService;
import com.br.UserResgistration.mapper.AddressMapper;
import com.br.UserResgistration.model.Addresses;
import com.br.UserResgistration.model.User;
import com.br.UserResgistration.repository.AddressRepository;
import com.br.UserResgistration.validation.AdressesValidation;

@Service
public class AddressService extends GenericAbstractServiceImpl {

    @Autowired
    private AdressesValidation validation;

    @Autowired
    private UserService userService;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ErrorExceptionService messagesCodeService;

    public AddressDetailDTO save(Integer userId, AddressDTO address) throws BusinessException {
        validation.validation(address);

        userService.getUserById(userId);

        var entityAndress = AddressMapper.toEntity(address);
        entityAndress.setUser(new User());
        entityAndress.getUser().setId(userId);

        var entityAndressNew = addressRepository.save(entityAndress);

        return AddressMapper.toDto(entityAndressNew);
    }

    public List<AddressDetailDTO> getListAddresses(Integer idUser) throws BusinessException {

        userService.getUserById(idUser);

        List<Addresses> listAddresses = addressRepository.findByUserId(idUser);

        return AddressMapper.toListDto(listAddresses);

    }

    public AddressDetailDTO getAddress(Integer idAddress) throws BusinessException {

        var entity = this.getAddressCheckingExistById(idAddress);

        return AddressMapper.toDto(entity);
    }

    public AddressDetailDTO putAddress(Integer idAddress, AddressDTO entrada) throws BusinessException {

        Addresses address = this.getAddressCheckingExistById(idAddress);

        updateField(address::getNeighborhood, entrada::getNeighborhood, address::setNeighborhood,
                this.validation::neighborhoodValidation);

        updateField(address::getCity, entrada::getCity, address::setCity, this.validation::cityValidation);
        updateField(address::getNumber, entrada::getNumber, address::setNumber, this.validation::numberValidation);
        updateField(address::getRoad, entrada::getRoad, address::setRoad, this.validation::roadValidation);
        updateField(address::getState, entrada::getState, address::setState, this.validation::stateValidation);
        updateField(address::getCep, entrada::getCep, address::setCep, this.validation::cepValidation);

        return AddressMapper.toDto(
                addressRepository.save(address));
    }

    private Addresses getAddressCheckingExistById(Integer addressId) {
        var entityOption = addressRepository.findById(addressId);
        Addresses result = null;
        if (entityOption.isPresent()) {
            result = entityOption.get();
        } else {
            messagesCodeService.showNotFound();
        }
        return result;
    }

    public void deleteAddress(Integer idAddress) throws BusinessException {
        addressRepository.deleteById(idAddress);
    }

}
