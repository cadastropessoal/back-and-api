package com.br.UserResgistration.service;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.br.UserResgistration.exceptions.BusinessException;

public abstract class GenericAbstractServiceImpl {

    public <T> void updateField(Supplier<T> currentField, Supplier<T> newField, Consumer<T> setter,
            Consumer<T> validator) throws BusinessException {
        if (newField.get() != null && !Objects.equals(currentField.get(), newField.get())) {
            validator.accept(newField.get());
            setter.accept(newField.get());
        }

    }

    public <T extends Enum<T>> void updateEnumField(Supplier<T> getter,
            Supplier<T> newValueSupplier,
            Consumer<T> setter,
            Consumer<T> validator) {
        T newValue = newValueSupplier.get();
        if (newValue != null && !newValue.equals(getter.get())) {
            validator.accept(newValue);
            setter.accept(newValue);
        }
    }

}
