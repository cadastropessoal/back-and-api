package com.br.UserResgistration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.exceptions.ErrorExceptionService;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

@Service
public class MailService {

    @Autowired
    private ErrorExceptionService errorExceptionService;

    private JavaMailSender mailSender;

    public MailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendEmailRecoverPassword(String para, String senha, String name) {

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(para);
            helper.setSubject("Recuperação de Senha");
            helper.setText("Olá " + name
                    + ",<br><br> Recebemos sua solicitação para redefinir a senha do seu login.<br>"
                    + "Aqui está a chave de redefinição:<br><br>"
                    + "<div style='text-align: center; font-size: 24px; font-weight: bold; color: black;'>"
                    + senha + "</div><br><br>Este email foi enviado pelo o LULU.<br><br> Não precisa respoder á mensagem.<br><br> Obrigado.", true);

            helper.setFrom("seuemail@gmail.com");

            mailSender.send(message);
        } catch (MessagingException e) {
            this.errorExceptionService.showUnauthorized();

        }

    }

    public void sendEmailConfirmPassword(String para, String name){
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(para);
            helper.setSubject("Confirmação de Senha");
            helper.setText("Olá " + name
                    + ",<br><br> Sua senha foi redefinida com sucesso.<br>"
                    + "Caso não tenha solicitado essa alteração, entre em contato com o suporte.<br><br>"
                    + "Este e-mail foi enviado pelo LULU.<br><br>Não é necessário responder a esta mensagem.<br><br>Obrigado.", true);

            helper.setFrom("seuemail@gmail.com");

            mailSender.send(message);
        } catch (MessagingException e) {
            this.errorExceptionService.showUnauthorized();

        }
    }
}
