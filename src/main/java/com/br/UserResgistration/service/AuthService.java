package com.br.UserResgistration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.br.UserResgistration.dto.AuthDTO;
import com.br.UserResgistration.dto.UserDetailAuthDTO;
import com.br.UserResgistration.dto.UserLoginDTO;
import com.br.UserResgistration.dto.UserRegisterDTO;
import com.br.UserResgistration.mapper.UserMapper;
import com.br.UserResgistration.model.User;

@Service
public class AuthService {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtService jwtService;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public UserDetailAuthDTO register(UserRegisterDTO dto) {
        userService.validation(UserMapper.toEntityRegister(dto));

        userService.checkIfEmailIsAlreadyRegistered(dto.getEmail());

        var user = UserMapper.toEntityRegister(dto);

        user.setPassword(encoder.encode(dto.getPassword()));

        return UserMapper.toEntityRegister(userService.saveRepository(user));
    }

    public AuthDTO login(UserLoginDTO dto) {

        User user = userService.getCheckingIfExistUserbyEmail(dto.getLogin());

        userService.validationPassword(dto.getPassword(), user.getPassword());

        var token = jwtService.generateToken(user.getId(), user.getEmail(), user.getName(), user.getProfile());
        var authDto = UserMapper.toAuthDto(user);
        authDto.setAcessToken(token);

        return authDto;
    }

    public void recoverPassword(String email){
        this.userService.recoverPassword(email);
    }

    public void resetPasswordToken(String email, String token, String newPassword){
        this.userService.resetPasswordToken(email, token, newPassword);
    }
}